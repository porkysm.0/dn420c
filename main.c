#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

typedef int(*_fn_exception_handler)(int index);

uint8_t clamp(const uint8_t val, const uint8_t min, const uint8_t max) {
    if (val > max) {
        return max;
    }

    if (val < min) {
        return min;
    }

    return val;
}

void attempt_keygen(uint8_t* output, const int key_len, const int desired_sum,
                    const _fn_exception_handler exception_handler) {
    memset(output, key_len, 0);

    // We need negative numbers
    int16_t temp_sum = desired_sum;

    for (int i = 0; i < key_len; i++) {
        const int exception_value = exception_handler(i);
        if (exception_value >= 0) {
            output[i] = (uint8_t)exception_value;
            continue;
        }

        if (i == key_len - 1) {
            if (temp_sum > 9 || temp_sum < 0) {
                return attempt_keygen(output, key_len, desired_sum, exception_handler);
            }

            output[i] = (uint8_t)temp_sum;
            continue;
        }

        uint8_t key_segment = clamp(rand() % 9, 1, 9);

        while (i < key_len && temp_sum - key_segment > 9 * (key_len - i - 1)) {
            const uint8_t delta = temp_sum - 9 * (key_len - i - 1);
            if (delta > 9) {
                return attempt_keygen(output, key_len, desired_sum, exception_handler);
            }

            key_segment = clamp(rand() % (10 - delta) + delta, 1, 9);
        }

        output[i] = key_segment;
        temp_sum -= key_segment;
    }
}

void format_data(const uint8_t* data, const int data_len, char* out) {
    int offset = 0;
    for (int i = 0; i < data_len; i++) {
        if (i != 0 && i % 3 == 0) {
            out[i+offset] = '-';
            offset++;
        }

        out[i+offset] = '0' + data[i];
    }
    out[data_len + (data_len/3) - 1] = 0;
}

int v1_exc_handler(int index) {
    if (index == 5) {
        return 7;
    }

    return -1;
}

int v2_exc_handler(int index) {
    if (index == 8) {
        return 1;
    }

    return -1;
}

int main(int argc, char* argv[]) {
    srand(time(0));

    if (argc != 3) {
        printf("usage: ./dn420c [version] [num_keys]\n");
        return -1;
    }

    const uint8_t version = atoi(argv[1]);
    const int64_t num_keys = atoi(argv[2]);

    int key_len;
    int desired_sum;
    _fn_exception_handler exception_handler;

    switch(version) {
        case 1:
            key_len = 9;
            desired_sum = 39;
            exception_handler = v1_exc_handler;
            break;
        case 2:
            key_len = 12;
            desired_sum = 60;
            exception_handler = v2_exc_handler;
            break;
        default:
            printf("invalid version number: %d\n", version);
            return -1;
    }

    uint8_t data[key_len];
    char text_buffer[key_len + (key_len / 3)];

    for (int i = 0; i < num_keys; i++) {
        attempt_keygen(data, key_len, desired_sum, exception_handler);

        format_data(data, key_len, text_buffer);
        printf("%s\n", text_buffer);
    }
    return 0;
}
